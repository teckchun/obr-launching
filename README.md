# rocket-launching

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Serve dist
```
serve -s dist
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Requirement
```
Node version 12.22.2
Vue version 2
```
